﻿using UnityEngine;
using System.Collections;

public class MainCharacterControler : MonoBehaviour {

	public float maxSpeed = 10f;
	bool facingRight = false;

//	public float moveSpeed;
//	private Vector3 moveDirection;

	// Use this for initialization
	void Start () {
		//moveDirection = Vector3.right;
	}

	void FixedUpdate(){
		float move = Input.GetAxis ("Horizontal");
		rigidbody2D.velocity = new Vector2 (move*maxSpeed, rigidbody2D.velocity.y);

		if(move>0 && !facingRight){
			Flip();
		} else if(move < 0 && facingRight){
			Flip ();
		}
	}
	
	// Update is called once per frame
//	void Update () {
//		// 1
//		Vector3 currentPosition = transform.position;
//		// 2
//		if( Input.GetButton("Fire1") ) {
//			// 3
//			Vector3 moveToward = Camera.main.ScreenToWorldPoint( Input.mousePosition );
//			// 4
//			moveDirection = moveToward - currentPosition;
//			moveDirection.z = 0; 
//			moveDirection.Normalize();
//
//			Vector3 target = moveDirection * moveSpeed + currentPosition;
//			transform.position = Vector3.Lerp( currentPosition, target, Time.deltaTime );
//		}
//	}

	void Flip(){
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

}
